import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { TodoService } from '../../services/todo.service'
import { Todo } from '../../models/Todo'

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.sass']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter()

  constructor(private todoService: TodoService) { }

  ngOnInit() {
  }

  setClasses() {
    return {
      todo: true,
      'is-complete': this.todo.completed
    }
  }

  onToggle(todo: Todo) {
    // toggle in UI
    todo.completed = !todo.completed
    // toggle in Server
    this.todoService.toggleCompleted(todo).subscribe(t => console.log(t))
  }

  onDelete(todo: Todo) {
    this.deleteTodo.emit(todo)
  }
}
