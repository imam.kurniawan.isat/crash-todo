import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title:string = 'Crash TODO';
  version:string = 'v1.0'

  constructor() {
  }

  setVersion(v:string):void {
    this.version = v
  }
}
